
from firedrake import *


##################################################

def plot_buoy(t, Z0, W0, buoy_file):

    time = str(t)
    Z = str(Z0)
    W = str(W0)
    buoy_file.write('%-10s %-10s %-10s\n' % (time,Z,W))

    return;

##################################################
