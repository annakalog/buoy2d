
from firedrake import *

##################################################

# Mixed variational problem for phi0_5, h1, lambda0_5, W0_5, Z1, and I=integral(lambda0_5*dx)
   
#def solver_F(phi1, h1, lambda1, Z1, W1, I, w, phi0, h0, lambda0, Z0, W0, phi_t, h_t, lambda_t, Z_t, W_t, I_t, v1, v2, v3, v4, v5, v6, dt, Hx, Hb, H0, L, dR_dt, g, rho, Mass, H, a, r, x, step_b, solvers_print):
def solver_F(phi0_5, h1, lambda0_5, Z1, W0_5, I, w, phi0, h0, Z0, W0, phi_t, h_t, lambda_t, Z_t, W_t, I_t, v1, v2, v3, v4, v5, v6, dt, Hx, Hb, H0, L, dR_dt, g, rho, Mass, H, a, r, x, step_b, solvers_print):

    '''
    # Modified-midpoint
    F_phi = v1*( (phi1-phi0)/dt + 0.5*inner(grad(0.5*(phi0+phi1)),grad(0.5*(phi0+phi1))) + g*(0.5*(h0+h1)-H0) \
                + 0.5*( (0.5*(lambda0+lambda1) + exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) - 1) \
                   + abs(0.5*(lambda0+lambda1) + exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) - 1) )*exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) )*dx

    F_h = ( v2*(h1-h0)/dt - 0.5*(h0+h1)*inner(grad(v2),grad(0.5*(phi0+phi1))) )*dx - 0.5*(h0+h1)*dR_dt*v2*ds(1) #ds_v(1)

    F_lambda = v3/dt*( 0.5*( (0.5*(lambda0+lambda1) + exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) - 1) \
                        + abs(0.5*(lambda0+lambda1) + exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) - 1) ) - 0.5*(lambda0+lambda1) )*dx

    F_Z = v4*( (Z1-Z0)/dt - 0.5*(W0+W1) )*dx

    F_W = v5*( (W1-W0)/dt + g - rho/Mass*I )*dx

    F_I = v6*( I/L - 0.5*( (0.5*(lambda0+lambda1) + exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) - 1) \
                      + abs(0.5*(lambda0+lambda1) + exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) - 1) )*exp(r*(0.5*(h0+h1)-(0.5*(Z0+Z1) - H - tan(a)*(x[0]-L)))) )*dx
    '''
    
    # Stormer-Verlet
    F_phi = v1*( (phi0_5-phi0)/(0.5*dt) + 0.5*inner(grad(phi0_5),grad(phi0_5)) + g*(h0-H0) \
                + 0.5*( (lambda0_5 + exp(r*(h0-(Z0 - H - tan(a)*(x[0]-L)))) - 1) \
                   + abs(lambda0_5 + exp(r*(h0-(Z0 - H - tan(a)*(x[0]-L)))) - 1) )*exp(r*(h0-(Z0 - H - tan(a)*(x[0]-L)))) )*dx
                    
    F_h = ( v2*(h1-h0)/dt - 0.5*(h0+h1)*inner(grad(v2),grad(phi0_5)) )*dx - 0.5*(h0+h1)*dR_dt*v2*ds(1) #ds_v(1)
                       
    F_lambda = v3/dt*( 0.5*( (lambda0_5 + exp(r*(h1-(Z1 - H - tan(a)*(x[0]-L)))) - 1) \
                        + abs(lambda0_5 + exp(r*(h1-(Z1 - H - tan(a)*(x[0]-L)))) - 1) ) - lambda0_5 )*dx
                       
    F_Z = v4*( (Z1-Z0)/dt - W0_5 )*dx
                       
    F_W = v5*( (W0_5-W0)/(0.5*dt) + g - rho/Mass*I )*dx
                       
    F_I = v6*( I/L - 0.5*( (lambda0_5 + exp(r*(h0-(Z0 - H - tan(a)*(x[0]-L)))) - 1) \
                      + abs(lambda0_5 + exp(r*(h0-(Z0 - H - tan(a)*(x[0]-L)))) - 1) )*exp(r*(h0-(Z0 - H - tan(a)*(x[0]-L)))) )*dx
                       
    F_solve = F_phi + F_h + F_lambda + F_W + F_Z + F_I
    
    # Preconditioner
    #Jp = v1*phi_t*dx + dt*(g*dt + exp(r*(Hx-Hb)))*Hx*inner(grad(v1),grad(phi_t))*dx \
    #   + v2*h_t*dx \
    #   + v3*dt*rho/Mass*exp(r*(Hx-Hb))*lambda_t*dx \
    #   + v4*Z_t*dx \
    #   + v5*W_t*dx \
    #   + v6*1.0/L*I_t*dx
    ### From linear system "Alternative_approach_with_scalars"
    #Jp = v1*phi_t*dx + dt*(g*0.5*dt + step_b)*0.5*dt*Hb*inner(grad(v1),grad(phi_t))*dx + ...
    Jp = v1*phi_t*dx + 0.5*dt*step_b*Hb*inner(grad(v1),grad(phi_t))*dx \
        + v2*h_t*dx \
        + v3*0.5*dt*rho/Mass*step_b*lambda_t*dx \
        + v4*Z_t*dx \
        + v5*W_t*dx \
        + v6*1.0/L*I_t*dx
    
    F_problem = NonlinearVariationalProblem(F_solve, w, Jp=Jp)
    F_solver = NonlinearVariationalSolver(F_problem, solver_parameters=solvers_print)

    return F_solver;

##################################################

# Variational problem for phi1

def solver_phi(phi1, phi0_5, h1, lambda0_5, Z1, phi, v, dt, H0, L, g, H, a, r, x):

    a_phi = ( v*phi )*dx
    L_phi = v*( phi0_5 - 0.5*dt*0.5*inner(grad(phi0_5),grad(phi0_5)) - 0.5*dt*g*(h1-H0) \
                       - 0.5*dt*0.5*( (lambda0_5 + exp(r*(h1-(Z1 - H - tan(a)*(x[0]-L)))) - 1) \
                                 + abs(lambda0_5 + exp(r*(h1-(Z1 - H - tan(a)*(x[0]-L)))) - 1) )*exp(r*(h1-(Z1 - H - tan(a)*(x[0]-L)))) )*dx
     
    phi_problem = LinearVariationalProblem(a_phi, L_phi, phi1)
    phi_solver = LinearVariationalSolver(phi_problem, solver_parameters={})
    
    return phi_solver;

##################################################

# Variational problem for W equation
def solver_W(W1, W0_5, I, W_tm, vR, g, rho, Mass, dt, solvers_print):
    
    a_W = ( vR*W_tm )*dx
    L_W = vR*( W0_5 - 0.5*dt*g + 0.5*dt*rho/Mass*I )*dx
    
    W_problem = LinearVariationalProblem(a_W, L_W, W1)
    W_solver = LinearVariationalSolver(W_problem, solver_parameters={})
    
    return W_solver;

##################################################

# Stormer-Verlet method
def solvers_SV(F_solver, phi_solver1, W_solver1, h0, phi0, Z0, W0, h1, phi1, Z1, W1, lambda0_5):
    
    F_solver.solve()			# Solve for phi0_5, h1, lambda0_5, Z1, W0_5, I
    
    phi_solver1.solve()			# Solve for phi1
    W_solver1.solve()           # Solve for W1
    
    phi0.assign(phi1)
    h0.assign(h1)
    W0.assign(W1)
    Z0.assign(Z1)
    
    return (phi0, h0, W0, Z0, lambda0_5);

##################################################

# Modified-midpoint method
def solvers_MM(F_solver, h0, phi0, Z0, W0, lambda0, h1, phi1, Z1, W1, lambda1):
	
    F_solver.solve()			# Solve for phi1, h1, lambda1, Z1, W1, I
    
    phi0.assign(phi1)
    h0.assign(h1)
    W0.assign(W1)
    Z0.assign(Z1)
    lambda0.assign(lambda1)

    return (phi0, h0, W0, Z0, lambda0);

##################################################

