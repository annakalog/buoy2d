
from firedrake import *
from math import *


##################################################

def get_solver_parameters():

    solvers_print = {"ksp_type": "gmres",
                     "ksp_gmres_modifiedgramschmidt": True,
                     "pc_type": "fieldsplit",
                     "pc_fieldsplit_type": "additive",
                     "fieldsplit_0_pc_type": "lu",
                     "fieldsplit_1_pc_type": "lu",
                     "fieldsplit_2_pc_type": "lu",
                     "fieldsplit_3_pc_type": "none",
                     "fieldsplit_4_pc_type": "none",
                     "fieldsplit_5_pc_type": "none",
                     "fieldsplit_0_ksp_type": "preonly",
                     "fieldsplit_1_ksp_type": "preonly",
                     "fieldsplit_2_ksp_type": "preonly",
                     "fieldsplit_3_ksp_type": "cg", #"gmres", #
                     "fieldsplit_4_ksp_type": "cg",
                     "fieldsplit_5_ksp_type": "cg",
                     "fieldsplit_2_pc_factor_shift_type": "nonzero",
                     "ksp_gmres_restart": 5000,
                     "snes_monitor": True,
                     #"ksp_monitor_true_residual": True,
                     "snes_converged_reason": True,
                     "ksp_converged_reason": True}

    return solvers_print;

##################################################

def parameter_values():

	# Dimensional parameters
    L  = 1.0	# m                 # length of domain
    H0 = 0.1	# m                 # water depth at rest
    g = 9.81	# m/s^2             # gravity
    rho = 997	# kg/m^3            # densirt of water
    Mass = 5.0	# kg                # mass of buoy

    # Space related constants
    lb = 1.0; lw = 4
    Lp = L-lb/lw*L					# position of waterline
    n = 40                          # integer to multiply lw in Nx definition
    Nx = n*lw                       # number of elements: integer multiple of lw, so that there is always a node at point Lp
    Np = Nx*(1-lb/lw)               # number of elements until point Lp
    a = atan(2*Mass/rho/(L-Lp)**2)	# angle between the hull and the horizontal (*180/pi in degrees)

	# Time related constants
    dt = L/(Nx*2*pi*sqrt(g*H0))     # time step
    T = 0.5                         # final time
    
    Np = int(Np)-1                  # in Python, indices start from 0
    
    return (L, H0, g, rho, Mass, a, Lp, Nx, Np, dt, T);

##################################################

def dispersion_relation(L, H0, g, Nx):
	
	# Wavenumber
    kx = 6

	# Dispersion relation
    k = kx*pi/L
    omega = k*sqrt(g*H0)

	# Wave amplitude
    Ampl = 0.2*omega*(L/Nx)

    return (Ampl, k, omega);

##################################################

# Define buoy's hull shape
def def_Hx(Hx, Hb, Zbar, x, V, L, Lp, H0, rho, Mass, a, Nx):
    
    Hx = interpolate(H0 - 0.5*(1.0+sign(x[0]-Lp))*(sqrt(2*Mass*tan(a)/rho) + tan(a)*(x[0]-L)), V)
    Hb = interpolate(0.5*(1.0+sign(x[0]-Lp))*(H0 - sqrt(2*Mass*tan(a)/rho) - tan(a)*(x[0]-L)), V)
    
    H = 2.0/3.0*(H0-Hx.dat.data[Nx]);                       # Distance between centre of mass and keel
    Zbar.dat.data[:] = H0 + H - sqrt(2*Mass*tan(a)/rho);    # Position of centre of mass at rest state
    
    return (Hx, Hb, H, Zbar);

##################################################
