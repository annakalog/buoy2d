
from firedrake import *


##################################################

def create_mesh(Nx, L):

    #m = IntervalMesh(Nx, L)
    #mesh = ExtrudedMesh(m, 10)
    mesh = IntervalMesh(Nx, L)
    coords = mesh.coordinates

    return (mesh, coords);

##################################################

