#
# Solve shallow-water equations (SWE) with a buoy
# using an inequality constraint and the modified Rockafellar approach.
#
# Last update: 26.05.17
# ---------------------
# Edited preconditioner to include term exp(r*(Hx-Hb)).
#

from firedrake import *
from parameters import *
from mesh import *
from solvers import *
from energy import *
from heaviside import *
from plot_buoy_motion import *
from write_to_vtk import *
import time

# Parameter values
solvers_print = get_solver_parameters();
(L, H0, g, rho, Mass, a, Lp, Nx, Np, dt, T) = parameter_values();
(Ampl, k, omega) = dispersion_relation(L, H0, g, Nx);

# Create mesh 
(mesh, x) = create_mesh(Nx, L);

# Define functions
V = FunctionSpace(mesh, "CG", 1)
V_R = FunctionSpace(mesh, "R", 0)
W = V * V * V * V_R * V_R * V_R

h0 = Function(V, name="h")
phi0 = Function(V, name="phi")
#lambda0 = Function(V, name="lambda")
lambda0_5 = Function(V, name="lambda")
phi1 = Function(V)

Z0 = Function(V_R)
W0 = Function(V_R)
W1 = Function(V_R)

hb = Function(V)
Hx = Function(V)
Hb = Function(V)
dR_dt = Function(V)
Zbar = Function(V_R)
xp = Constant(Lp)

##
V_DG0 = FunctionSpace(mesh, "DG", 0)
step_b = Function(V_DG0)
##

v = TestFunction(V)
phi = TrialFunction(V)
vR = TestFunction(V_R)
W_tm = TrialFunction(V_R)

w = Function(W)
v1, v2, v3, v4, v5, v6 = TestFunctions(W)                   # Test functions
phi_t, h_t, lambda_t, Z_t, W_t, I_t = TrialFunctions(W)     # Trial functions
#phi1, h1, lambda1, Z1, W1, I = split(w)                    # Solutions
phi0_5, h1, lambda0_5, Z1, W0_5, I = split(w)                 # Solutions

# Buoy's hull shape at floating rest state
(Hx, Hb, H, Zbar) = def_Hx(Hx, Hb, Zbar, x, V, L, Lp, H0, rho, Mass, a, Nx);

# Heaviside step function
step_b = def_step(step_b, x, Lp, 'True');

# Initial condition
t = 0
dR_dt = interpolate(Constant(Ampl*sin(omega*t)), V)
Z0.assign(Zbar)
h0.assign(Hx)
r = 1 # Penalty parameter

# Weak formulation
#F_solver = solver_F(phi1, h1, lambda1, Z1, W1, I, w, phi0, h0, lambda0, Z0, W0, phi_t, h_t, lambda_t, Z_t, W_t, I_t, v1, v2, v3, v4, v5, v6, dt, Hx, Hb, H0, L, dR_dt, g, rho, Mass, H, a, r, x, step_b, solvers_print);
F_solver = solver_F(phi0_5, h1, lambda0_5, Z1, W0_5, I, w, phi0, h0, Z0, W0, phi_t, h_t, lambda_t, Z_t, W_t, I_t, v1, v2, v3, v4, v5, v6, dt, Hx, Hb, H0, L, dR_dt, g, rho, Mass, H, a, r, x, step_b, solvers_print);
phi_solver1 = solver_phi(phi1, phi0_5, h1, lambda0_5, Z1, phi, v, dt, H0, L, g, H, a, r, x);
W_solver1 = solver_W(W1, W0_5, I, W_tm, vR, g, rho, Mass, dt, solvers_print);

#phi1, h1, lambda1, Z1, W1, I = w.split()
phi0_5, h1, lambda0_5, Z1, W1, I = w.split()

# Write data to files
output_file = File("Results/results.pvd")
output_file.write(phi0, h0, lambda0_5, time=t)

buoy_file = open("Results/buoy_motion.txt", "w")
plot_buoy(t, Z0.dat.data[0], W0.dat.data[0], buoy_file);

# Initial energy
E0_w = 0.5*rho*assemble( (Hx*abs(grad(phi0))**2 + g*(h0-H0)**2)*dx )    # Energy of the water
E0_b = 0.5*Mass*assemble( W0**2*dx ) + Mass*g*assemble( Z0*dx )         # Energy of the buoy
E0   = E0_w + E0_b                                                      # Total energy: water + buoy

E_file = open("Results/energy.txt", "w")
E = find_energy(phi0, h0, W0, Z0, Hx, H0, rho, g, Mass, t, E0, E_file);

while(t < T-dt):
    print(t, E)

    # Time update of wave maker R(t) = A/w*(1-cos(w*t))
    #dR_dt.interpolate(Expression("0"))
    #dR_dt_expr.t = t+0.5*dt
    #dR_dt.interpolate(dR_dt_expr)
    dR_dt = interpolate(Constant(Ampl*sin(omega*(t+0.5*dt))), V)

    t += dt

	# Solve the variational problem
    #(phi0, h0, W0, Z0, lambda0) = solvers_MM(F_solver, h0, phi0, Z0, W0, lambda0, h1, phi1, Z1, W1, lambda1);
    (phi0, h0, W0, Z0, lambda0_5) = solvers_SV(F_solver, phi_solver1, W_solver1, h0, phi0, Z0, W0, h1, phi1, Z1, W1, lambda0_5);

	# Monitor energy in time
    E = find_energy(phi0, h0, W0, Z0, Hx, H0, rho, g, Mass, t, E0, E_file);

	# Write data to files
    output_file.write(phi0, h0, lambda0_5, time=t)
    plot_buoy(t, Z0.dat.data[0], W0.dat.data[0], buoy_file);

E_file.close()
buoy_file.close()

