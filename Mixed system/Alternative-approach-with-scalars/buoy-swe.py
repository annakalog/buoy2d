#
# Solve shallow-water equations (SWE) with a buoy.
#
# Last update: 17.02.16
# ---------------------
# Form a mixed system and solve system of unknowns
# phi0_5, eta1, mu0_5, W0_5, Z1 together.
#

from firedrake import *
from parameters import *
from mesh import *
from IC_etaR import *
from solvers import *
from energy import *
from heaviside import *
from plot_buoy_motion import *
from write_to_vtk import *
import time

# Parameter values
(sluice_gate, wave_maker, solvers_print) = get_cases();
(L, H0, g, rho, Mass, a, Lp, Nx, Np, dt, T) = parameter_values();
if sluice_gate == 'True': (x1, x2, h1, Ts) = etaR_input(H0);
if wave_maker == 'True': (Ampl, k, omega) = dispersion_relation(L, H0, g, Nx);

# Create mesh 
(mesh, coords) = create_mesh(Nx, L);

# Define functions
V = FunctionSpace(mesh, "CG", 1)
V_DG0 = FunctionSpace(mesh, "DG", 0)
V_R = FunctionSpace(mesh, "R", 0)
W = V * V * V * V_R * V_R * V_R

eta0 = Function(V)
phi0 = Function(V)
#eta1   = Function(V)
phi1 = Function(V)
#phi0_5 = Function(V)
#mu0_5 = Function(V)
lambda0_5 = Function(V)
h = Function(V)

#I = Function(V_R)               # Function from the real space holding the integral of lambda.

Hb = Function(V)
etaR = Function(V)
dR_dt = Function(V)

step_b = Function(V_DG0)		# Heavyside step function: 0 in water part, 1 in buoy part.
step_w = Function(V_DG0)		# Heavyside step function: 1 in water part, 0 in buoy part.

Z0   = Function(V_R) #Constant(0.0)
W0   = Function(V_R) #Constant(0.0)
#Z1   = Function(V_R) #Constant(0.0)
W1   = Function(V_R) #Constant(0.0)
#W0_5 = Function(V_R) #Constant(0.0)
xp   = Constant(Lp)

eta = TrialFunction(V)
phi = TrialFunction(V)
mu = TrialFunction(V)
v  = TestFunction(V)
vR = TestFunction(V_R)
W_tm = TrialFunction(V_R)

w = Function(W)
phi_t, eta_t, mu_t, Z_t, W_t, I_t = TrialFunctions(W)   # Trial functions
v1, v2, v3, v4, v5, v6 = TestFunctions(W)               # Test functions
phi0_5, eta1, mu0_5, Z1, W0_5, I = split(w)             # Solutions

# Buoy's hull shape
(Hb, H, Zbar) = def_Hb(Hb, coords.dat.data, L, Lp, H0, rho, Mass, a, Nx);

# Heavyside step function
step_b = def_step(step_b, coords.dat.data, Lp, 'True');
step_w = def_step(step_w, coords.dat.data, Lp, 'False');

# Time iteration
t = 0
if sluice_gate == 'True': set_E0 = 'True'
else: set_E0 = 'False'

# Initial condition for sluice gate
if sluice_gate == 'True':
    etaR_expr = def_etaR(coords.dat.data, H0, h1, x1, x2, Ts, t);
    etaR.interpolate(etaR_expr)
    eta0 = eta0_eq_etaR(eta,eta0,etaR,v);
elif wave_maker == 'True':
    dR_dt_expr = Expression("A*sin(w*t)", A=Ampl, w=omega, t=t)

# Weak formulation
F_solver = solver_F(phi0_5, eta1, mu0_5, Z1, W0_5, I, w, phi0, eta0, Z0, W0, step_b, etaR, phi_t, eta_t, mu_t, Z_t, W_t, I_t, v1, v2, v3, v4, v5, v6, dt, Hb, H0, L, dR_dt, g, rho, Mass, solvers_print);
phi_solver1 = solver_phi(phi1, phi0_5, phi0_5, eta1, mu0_5, step_b, etaR, phi, v, 0.5*dt, g, solvers_print);    # Give eta1 instead of eta0 and phi0_5 instead of phi0
W_solver1 = solver_W(W1, W0_5, I, W_tm, vR, rho, Mass, solvers_print);

phi0_5, eta1, mu0_5, Z1, W0_5, I = w.split()

lambda0_5.assign((2/dt)*mu0_5)
h.assign(Hb+eta0)

# Write data to files
output_file = File("Results/results.pvd")
output_file.write(phi0, eta0, h, lambda0_5, time=t)

buoy_file = open("Results/buoy_motion.txt", "w")
plot_buoy(t, Z0, W0, buoy_file);

frame_number = 0
write_to_vtk_buoy(0.9*Lp, L, H0-sqrt(2*Mass*tan(a)/rho)-tan(a)*(0.9*Lp-L), Hb.dat.data[Nx], "Triangle Buoy", frame_number);
write_to_vtk_waterline(float(xp), H0, "Waterline", frame_number);

# Initial energy
E0_w = 0.5*rho*assemble( (Hb*abs(grad(phi0))**2 + g*eta0**2)*dx )   # Energy of the water
E0_b = 0.5*Mass*assemble( W0**2*dx )                                # Energy of the buoy
E0   = E0_w + E0_b				                                    # Total energy: water + buoy

E_file = open("Results/energy.txt", "w")
E = find_energy(phi0, eta0, W0, Z0, Hb, rho, g, Mass, t, E0, E_file);

while(t < T-dt):
    print t, E

    # Time update of sluice gate etaR and wave maker R(t) = A/w*(1-cos(w*t))
    etaR.interpolate(Expression("0"))
    dR_dt.interpolate(Expression("0"))

    if (sluice_gate == 'True' and t < Ts):
        etaR_expr.t = t+0.5*dt
        etaR.interpolate(etaR_expr)
    elif wave_maker == 'True':
        dR_dt_expr.t = t+0.5*dt
        dR_dt.interpolate(dR_dt_expr)

    t += dt

	# Solve the variational problem
    (phi0, eta0, W0, Z0, mu0_5) = solvers_SV(F_solver, phi_solver1, W_solver1, eta0, phi0, Z0, W0, mu0_5, eta1, phi1, Z1, W1);

    lambda0_5.assign((2/dt)*mu0_5)
    h.assign(Hb+eta0)

    # Plot buoy
    frame_number = frame_number + 1
    write_to_vtk_buoy(0.9*Lp, L, H0-sqrt(2*Mass*tan(a)/rho)-tan(a)*(0.9*Lp-L)+eta0.dat.data[Np], Hb.dat.data[Nx]+eta0.dat.data[Nx], "Triangle Buoy", frame_number);

    # Calculate and plot waterline point
    xp.assign(Lp + (Z0.dat.data - eta0.dat.data[Np])/tan(a))
    write_to_vtk_waterline(float(xp), H0+eta0.dat.data[Np], "Waterline", frame_number);

	# Monitor energy in time
    if sluice_gate == 'True':
        if (t > Ts and set_E0 == 'True'):    # As soon the sluice gate is released (i.e. t>Ts), calculate E0 again
    	    E0 = 0.5*rho*assemble( (Hb*abs(grad(phi0))**2 + g*eta0**2)*dx ) + 0.5*Mass*assemble( W0**2*dx )
            set_E0 = 'False'

    E = find_energy(phi0, eta0, W0, Z0, Hb, rho, g, Mass, t, E0, E_file);

	# Write data to files
    output_file.write(phi0, eta0, h, lambda0_5, time=t)
    plot_buoy(t, Z0, W0, buoy_file);

E_file.close()
buoy_file.close()

