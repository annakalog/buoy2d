
from firedrake import *
from math import *


##################################################

def get_cases():

    sluice_gate = 'False'   # 'True' for sluice gate IC
    wave_maker = 'True'     # 'True' if the wave maker is switched on
    solvers_print = {"ksp_type": "gmres",
                     "ksp_gmres_modifiedgramschmidt": True,
                     "pc_type": "fieldsplit",
                     "pc_fieldsplit_type": "additive",
                     "fieldsplit_0_pc_type": "lu",
                     "fieldsplit_1_pc_type": "lu",
                     "fieldsplit_2_pc_type": "lu",
                     "fieldsplit_3_pc_type": "none",
                     "fieldsplit_4_pc_type": "none",
                     "fieldsplit_5_pc_type": "none",
                     "fieldsplit_0_ksp_type": "preonly",
                     "fieldsplit_1_ksp_type": "preonly",
                     "fieldsplit_2_ksp_type": "preonly",
                     "fieldsplit_3_ksp_type": "cg",
                     "fieldsplit_4_ksp_type": "cg",
                     "fieldsplit_5_ksp_type": "cg",
                     "fieldsplit_2_pc_factor_shift_type": "nonzero",
                     "ksp_gmres_restart": 100}

    return (sluice_gate, wave_maker, solvers_print);

##################################################

def parameter_values():

	# Dimensional parameters
    L  = 1.0	# m                 # length of domain
    H0 = 0.1	# m                 # water depth at rest
    g = 9.81	# m/s^2             # gravity
    rho = 997	# kg/m^3            # densirt of water
    Mass = 5.0	# kg                # mass of buoy

    # Space related constants
    lb = 1.0; lw = 4
    Lp = L-lb/lw*L					# position of waterline
    n = 40                          # integer to multiply lw in Nx definition
    Nx = n*lw                       # number of elements: integer multiple of lw, so that there is always a node at point Lp
    Np = Nx*(1-lb/lw)               # number of elements until point Lp
    a = atan(2*Mass/rho/(L-Lp)**2)	# angle between the hull and the horizontal (*180/pi in degrees)

	# Time related constants
    dt = L/(Nx*2*pi*sqrt(g*H0))     # time step
    T = 2.0                         # final time
    
    Np = int(Np)-1                  # in Python, indices start from 0
    
    return (L, H0, g, rho, Mass, a, Lp, Nx, Np, dt, T);

##################################################

def etaR_input(H0):

    x1 = 0.15
    x2 = 0.16
    h1 = 1.2*H0
    Ts = 0.5

    return (x1, x2, h1, Ts);

##################################################

def dispersion_relation(L, H0, g, Nx):
	
	# Wavenumber
    kx = 6

	# Dispersion relation
    k = kx*pi/L
    omega = k*sqrt(g*H0)

	# Wave amplitude
    Ampl = 0.2*omega*(L/Nx)

    return (Ampl, k, omega);

##################################################

# Define buoy's hull shape
def def_Hb(Hb, x, L, Lp, H0, rho, Mass, a, Nx):
	
    Hb.interpolate(Expression("H0 - 0.5*(1.0+copysign(1.0,x[0]-Lp))*(sqrt(2*M*tan(a)/rho)+tan(a)*(x[0]-L))", H0=H0, Lp=Lp, M=Mass, a=a, rho=rho, L=L))

    H = 2.0/3.0*(H0-Hb.dat.data[Nx]);         # Distance between centre of mass and keel
    Zbar = H0 + H - sqrt(2*Mass*tan(a)/rho);  # Position of centre of mass at rest state
	
    return (Hb, H, Zbar);

##################################################

