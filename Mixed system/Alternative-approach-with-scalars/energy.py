
from firedrake import *


##################################################

def find_energy(phi0, eta0, W0, Z0, Hb, rho, g, Mass, t, E0, E_file):

    E_w = 0.5*rho*assemble( (Hb*abs(grad(phi0))**2 + g*eta0**2)*dx )   # Energy of the water
    E_b = 0.5*Mass*assemble( W0**2*dx )		# Energy of the buoy
    E   = E_w + E_b                         # Total energy: water + buoy

    time = str(t)
    Ew = str(E_w)
    Eb = str(E_b)
    Et = str(E)
    Ed = str(E-E0)			        # Energy difference from initial value E0
    E_file.write('%-10s %-10s %-10s %-10s %-10s\n' % (time,Ed,Et,Ew,Eb))

    return E;

##################################################

