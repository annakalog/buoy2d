
from firedrake import *
from firedrake.petsc import PETSc
import numpy as np
from pyop2.profiling import timed_stage


##################################################

# Mixed variational problem for phi0_5, eta1, mu0_5, I=integral(step_b*mu0_5*dx)
   
def solver_F(phi0_5, eta1, mu0_5, Z1, W0_5, I, w, phi0, eta0, Z0, W0, step_b, etaR, phi_t, eta_t, mu_t, Z_t, W_t, I_t, v1, v2, v3, v4, v5, v6, dt, Hb, H0, L, dR_dt, g, rho, Mass, solvers_print):
    
    aphi = v1*(phi_t - step_b*mu_t)*dx
    Lphi = v1*(phi0 - 0.5*dt*g*(eta0-etaR))*dx

    aeta = (v2*eta_t - dt*Hb*inner(grad(v2),grad(phi_t)))*dx
    Leta = v2*eta0*dx + dt*H0*dR_dt*v2*ds_v(1)

    alambda = v3*step_b/dt*(eta_t - Z_t)*dx

    aZ = v4*(Z_t - dt*W_t)*dx
    LZ = v4*Z0*dx
    
    aW = v5*(W_t + rho/Mass*I_t)*dx
    LW = v5*W0*dx

    #I = assemble(step_b*mu*dx) = const. # I_t is the trial function
    aI = v6*(I_t/L-step_b*mu_t)*dx

    a_solve = aphi + aeta + alambda + aZ + aW + aI
    L_solve = Lphi + Leta + LZ + LW
    
    Ap = v1*phi_t*dx + step_b*Hb*inner(grad(v1),grad(phi_t))*dx \
        + v2*eta_t*dx \
        + v3*rho/Mass*step_b*mu_t*dx \
        + v4*Z_t*dx \
        + v5*W_t*dx \
        + v6*1.0/L*I_t*dx
    
    F_problem = LinearVariationalProblem(a_solve, L_solve, w, aP=Ap)
    F_solver = LinearVariationalSolver(F_problem, solver_parameters=solvers_print)

    return F_solver;

##################################################

# Variational problem for phi equation
def solver_phi(phi1, phi0_5, phi0, eta0, mu0_5, step_b, etaR, phi, v, dt, g, solvers_print):

	# Note: For the SV method, give      0.5*dt	 instead of dt.
	# 	-> If in addition give 	     	 phi0_5	  	"		phi1,	then this is the first half-step of the SV.
	#	-> If in addition give	     	 phi0_5	  	"		phi0,
	#		          	  and	     	 eta1  	  	"		eta0,	then this is the second half-step of the SV.
    aphi = ( v*phi )*dx
    Lphi = ( v*phi0 - dt*g*v*(eta0-etaR) + step_b*v*mu0_5 )*dx

    phi_problem = LinearVariationalProblem(aphi, Lphi, phi1)
    phi_solver = LinearVariationalSolver(phi_problem, solver_parameters={})

    return phi_solver;

##################################################

# Variational problem for W equation
def solver_W(W1, W0_5, I, W_tm, vR, rho, Mass, solvers_print):

	# Note: For the SV method, give    	 W0_5	  	"		W1,		then this is the first half-step of the SV.
	#						   give	     W0_5	  	"		W0,		then this is the second half-step of the SV.

    #W1.assign(W0 - rho/Mass*assemble(step_b*mu0_5*dx))
    aW = ( vR*W_tm )*dx
    LW = ( vR*W0_5 - rho/Mass*vR*I )*dx
        
    W_problem = LinearVariationalProblem(aW, LW, W1)
    W_solver = LinearVariationalSolver(W_problem, solver_parameters={})

    return W_solver;

##################################################

# 2nd-order Stormer-Verlet solvers
def solvers_SV(F_solver, phi_solver1, W_solver1, eta0, phi0, Z0, W0, mu0_5, eta1, phi1, Z1, W1):
	
    F_solver.solve()			# Solve for phi0_5, eta1, mu0_5, Z0, W0_5
    phi_solver1.solve()			# Solve for phi1
    W_solver1.solve()           # Solve for W1
    
    phi0.assign(phi1)
    eta0.assign(eta1)
    W0.assign(W1)
    Z0.assign(Z1)

    return (phi0, eta0, W0, Z0, mu0_5);

##################################################

