
from firedrake import *


##################################################

# Define heavyside step function
def def_step(Hx, x, Lp, buoy_part):
	
	if buoy_part == 'True':
		Hx_expr = Expression("0.5*(1.0+copysign(1.0,x[0]-Lp))", Lp=Lp)
	elif buoy_part == 'False':
		Hx_expr = Expression("0.5*(1.0+copysign(1.0,Lp-x[0]))", Lp=Lp)
	
	Hx.interpolate(Hx_expr)
	
	return Hx;

##################################################