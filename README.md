# Buoy2D

Variational model for linear shallow water waves that couples the dynamics of the waves to a buoy. The buoy is allowed to move only in the vertical and has a simple linear hull structure.

Implementation using Firedrake.


Instructions
------------
In the terminal, go in the directory where the source files are located and type:

python buoy-swe.py