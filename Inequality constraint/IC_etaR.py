
from firedrake import *


##################################################

# Define gravitational potential etaR
def def_etaR(x, H0, h1, x1, x2, Ts, t):

	etaR_expr = Expression("(h1-H0)*(Ts-t)/Ts*(0.5*(1+copysign(1.0,x1-x[0])) + 0.25*(1+copysign(1.0,x2-x[0]))*(1+copysign(1.0,x[0]-x1))*(1-(x[0]-x1)/(x2-x1)))", x1=x1, x2=x2, h1=h1, H0=H0, Ts=Ts, t=t)

	return etaR_expr;

##################################################

# Solve weak form of linear problem eta = etaR for initial eta0
def eta0_eq_etaR(eta,eta0,etaR,v):

	aetaR = v*eta*dx
	LetaR = v*etaR*dx
	etaR_problem = LinearVariationalProblem(aetaR,LetaR,eta0)
	etaR_solver = LinearVariationalSolver(etaR_problem)
	etaR_solver.solve()

	return eta0;

##################################################

