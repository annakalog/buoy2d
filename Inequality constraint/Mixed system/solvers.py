
from firedrake import *
from firedrake.petsc import PETSc
import numpy as np


##################################################

# Mixed variational problem for phi0_5, eta1, mu0_5, I=integral(lambda0_5*dx)
   
def solver_F(phi0_5, eta1, lambda0_5, mu0_5, I, w, phi0, eta0, Z0, W0, etaR, phi, eta, lambda_t, mu_t, I_t, v1, v2, v3, v4, v5, dt, Hb, H0, L, dR_dt, lambda_bar, mu_bar, g, rho, Mass, solvers_print):
 
    aphi = v1*(phi - lambda_t)*dx
    Lphi = v1*(phi0 - 0.5*dt*g*(eta0-etaR))*dx

    aeta = (v2*eta - dt*Hb*inner(grad(v2),grad(phi)))*dx
    Leta = v2*eta0*dx + dt*H0*dR_dt*v2*ds(1) #ds_v(1)

    alambda = v3*(eta/dt + rho/Mass*I_t + mu_bar*mu_t)*dx
    Llambda = v3*(Z0/dt + W0)*dx

    amu = v4*(mu_bar*lambda_t + lambda_bar*mu_t)*dx

    #I = assemble(mu*dx) = const. # I_t is the trial function
    aI = v5*(I_t/L-lambda_t)*dx

    a_solve = aphi + aeta + alambda + amu + aI
    L_solve = Lphi + Leta + Llambda

    Ap = v1*phi*dx + Hb*inner(grad(v1),grad(phi))*dx \
       + v2*eta*dx \
       + v3*(rho/Mass-mu_bar**2)*lambda_t*dx \
       + v4*lambda_bar*mu_t*dx \
       + v5*1.0/L*I_t*dx

    F_problem = LinearVariationalProblem(a_solve, L_solve, w, aP=Ap)
    F_solver = LinearVariationalSolver(F_problem, solver_parameters=solvers_print)

    return F_solver;

##################################################

# Variational problem for phi equation
def solver_phi(phi1, phi0_5, phi0, eta0, mu0_5, etaR, phi, v, dt, g, solvers_print):

	# Note: For the SV method, give      0.5*dt	 instead of dt.
	# 	-> If in addition give 	     	 phi0_5	  	"		phi1,	then this is the first half-step of the SV.
	#	-> If in addition give	     	 phi0_5	  	"		phi0,
	#		          	  and	     	 eta1  	  	"		eta0,	then this is the second half-step of the SV.
    aphi = ( v*phi )*dx
    Lphi = ( v*phi0 - dt*g*v*(eta0-etaR) + v*mu0_5 )*dx

    phi_problem = LinearVariationalProblem(aphi, Lphi, phi1)
    phi_solver = LinearVariationalSolver(phi_problem, solver_parameters={})

    return phi_solver;

# Variational problem for eta equation
def solver_eta(eta1, eta0_5, eta0, phi0_5, eta, v, dt, Hb, H0, dR_dt, solvers_print):

	# Note: For the SV method, give      eta1	   instead of	eta0_5.
    aeta = ( v*eta )*dx
    Leta = ( v*eta0 + dt*Hb*inner(grad(v),grad(phi0_5)) )*dx + dt*H0*dR_dt*v*ds(1) #ds_v(1)

    eta_problem = LinearVariationalProblem(aeta, Leta, eta1)
    eta_solver = LinearVariationalSolver(eta_problem, solver_parameters={})
    
    return eta_solver;

##################################################

# Variational problem for lambda equation
def solver_mu(eta0, phi0, Z0, W0, LM_0_5, mu, v, v1, dt, Hb, lambda_bar, mu_bar, g, rho, Mass, Nx):

    # Define linear and bilinear forms
    M_form = mu*v*dx
    A_form = Hb*inner(grad(mu),grad(v))*dx
    Q_form = v*dx
    B_mu_form = (2/dt)*mu_bar*mu*v*dx
    C_lam_form = lambda_bar*mu*v*dx

    # Get reference to PETSc Matrices
    M = assemble(M_form).M.handle
    A = assemble(A_form).M.handle
    B_mu = assemble(B_mu_form).M.handle
    C_lam = assemble(C_lam_form).M.handle

    #print 'Matrix B_mu \n-----------\n' + str(B_mu.getValues(range(Nx+1), range(Nx+1))) + ' \n'
    # Something wrong with B_mu matrix... Indices are slightly different from Matlab matrix

    # Get reference to PETSc Vector Q (take a copy here)
    Qf = assemble(Q_form)
    with Qf.dat.vec_ro as vec:
        Q = vec

    # Build the matrix free operator B
    class MatrixFreeB(object):
        def __init__(self, A, Q1, Q2):
            self.A = A
            self.Q1 = Q1
            self.Q2 = Q2

        # Compute y = alpha Q1 + y = Q2^T*Q1 + A*x
        def mult(self, mat, x, y):
            self.A.mult(x, y)
            alpha = self.Q2.dot(x) 
            y.axpy(alpha, self.Q1)

    # Define MatrixFreeB class operator B
    B = PETSc.Mat().create()
    B.setSizes(*A.getSizes())
    B.setType(B.Type.PYTHON)
    B.setPythonContext(MatrixFreeB(A, rho/Mass*Q, Q))
    B.setUp()
    '''
    # Print LHS 'matrix' B to inspect its values
    ctx = B.getPythonContext()
    dense_A = ctx.A[:, :]
    Q_array = ctx.Q2[:]
    dense_B = dense_A + rho/Mass*np.outer(Q_array, Q_array)
    print 'LHS matrix A + Q*Q^T \n------------\n' + str( dense_B ) + ' \n'
    '''
    # Define combined matrix
    mixed_matrix = PETSc.Mat().createNest([[B, B_mu], [B_mu, C_lam]])

    # For small problems, we don't need a preconditioner at all
    mu_solver = PETSc.KSP().create()
    mu_solver.setOperators(mixed_matrix)
    opts = PETSc.Options()
    opts["pc_type"] = "none"
    mu_solver.setUp()
    mu_solver.setFromOptions()

    # Define RHS
    rhs = assemble(( -v1*eta0/dt + v1*(Z0/dt + W0) + Hb*inner(grad(v1),grad(0.5*dt*g*eta0-phi0)) )*dx)

    # Solve the linear system
    with rhs.dat.vec_ro as b:
        with LM_0_5.dat.vec as x:
            mu_solver.solve(b, x)

    return (mu_solver, LM_0_5);

##################################################

# Variational problem for W equation
def solver_W(W1, W0, mu0_5, rho, Mass):

	# Note: For the SV method, give    	 W0_5	  	"		W1,		then this is the first half-step of the SV.
	#						   give	     W0_5	  	"		W0,		then this is the second half-step of the SV.
	W1.assign(W0 - rho/Mass*assemble(mu0_5*dx))

	return;

# Variational problem for Z equation
def solver_Z(Z1, Z0, W0_5, dt):

	Z1.assign(Z0 + dt*W0_5)

	return;

##################################################

# 2nd-order Stormer-Verlet solvers
def solvers_SV(w, F_solver, phi_solver1, eta0, phi0, Z0, W0, eta1, phi1, Z1, W1, W0_5, rho, Mass, dt):

    F_solver.solve()						    # Solve for phi0_5, eta1, lambda0_5, mu0_5

    phi0_5, eta1, LM_lam0_5, LM_mu0_5, I = w.split()

    solver_W(W0_5, W0, LM_lam0_5, rho, Mass);   # Solve for W0_5
    solver_Z(Z1, Z0, W0_5, dt);				    # Solve for Z1
    phi_solver1.solve()						    # Solve for phi1
    solver_W(W1, W0_5, LM_lam0_5, rho, Mass);	# Solve for W1

    phi0.assign(phi1)
    eta0.assign(eta1)
    W0.assign(W1)
    Z0.assign(Z1)

    return (phi0, eta0, W0, Z0, LM_lam0_5, LM_mu0_5);

##################################################

