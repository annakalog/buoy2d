
from firedrake import *


##################################################

def create_mesh(Nx, L):

    mesh = IntervalMesh(Nx, L)
    #m = IntervalMesh(Nx, L)
    #mesh = ExtrudedMesh(m, 10)
    coords = mesh.coordinates

    return (mesh, coords);

##################################################

