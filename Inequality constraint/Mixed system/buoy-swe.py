#
# Solve shallow water equations (SWE) with a buoy, using linearised inequality constraint.
#
# Last update: 14.11.16 (to include preconditioner)
#
from firedrake import *
from parameters import *
from mesh import *
from IC_etaR import *
from solvers import *
from energy import *
from plot_buoy_motion import *
from write_to_vtk import *
import time

# Parameter values
(sluice_gate, wave_maker, solvers_print) = get_cases();
(L, H0, g, rho, Mass, a, Lp, Nx, Np, dt, T) = parameter_values();
if sluice_gate == 'True': (x1, x2, h1, Ts) = etaR_input(H0);
if wave_maker == 'True': (Ampl, k, omega) = dispersion_relation(L, H0, g, Nx);

# Create mesh 
(mesh, coords) = create_mesh(Nx, L);

# Define functions
V = FunctionSpace(mesh, "CG", 1)
V_R = FunctionSpace(mesh, "R", 0)
W = V*V*V*V*V_R

eta0 = Function(V, name="eta")
phi0 = Function(V, name="phi")
eta1   = Function(V)
phi1 = Function(V)
phi0_5 = Function(V)
h = Function(V, name="h")
lambda0_5 = Function(V, name="lambda")
mu0_5 = Function(V, name="mu")

I = Function(V_R)               # Function from the real space holding the integral of lambda.

Hb = Function(V)
mu_bar = Function(V)
lambda_bar = Function(V)
etaR = Function(V)
dR_dt = Function(V)

Z0   = Constant(0.0)
W0   = Constant(0.0)
Z1   = Constant(0.0)
W1   = Constant(0.0)
W0_5 = Constant(0.0)
xp   = Constant(Lp)

eta = TrialFunction(V)
phi = TrialFunction(V)
mu = TrialFunction(V)
v = TestFunction(V)

w = Function(W)
phi_t, eta_t, lambda_t, mu_t, I_t = TrialFunctions(W)   # Trial functions
v1, v2, v3, v4, v5 = TestFunctions(W)                   # Test functions
phi0_5, eta1, LM_lam0_5, LM_mu0_5, I = split(w)         # Solutions

# Buoy's hull shape and Lagrange multipliers
(Hb, H, Zbar) = def_Hb(Hb, coords.dat.data, L, Lp, H0, rho, Mass, a, Nx);
(lambda_bar, mu_bar) = def_lam_mu_bar(lambda_bar, mu_bar, coords.dat.data, L, Lp, H0, g, rho, Mass, a);

# Time iteration
t = 0
if sluice_gate == 'True': set_E0 = 'True'
else: set_E0 = 'False'

# Initial condition for sluice gate
if sluice_gate == 'True':
    etaR_expr = def_etaR(coords.dat.data, H0, h1, x1, x2, Ts, t);
    etaR_interpolator = Interpolator(etaR_expr, etaR)
    etaR_interpolator.interpolate()
    eta0 = eta0_eq_etaR(eta,eta0,etaR,v);
elif wave_maker == 'True':
    dR_dt_expr = Expression("A*sin(w*t)", A=Ampl, w=omega, t=t)
    dR_dt_interpolator = Interpolator(dR_dt_expr, dR_dt)

# Weak formulation
F_solver = solver_F(phi0_5, eta1, lambda0_5, mu0_5, I, w, phi0, eta0, Z0, W0, etaR, phi_t, eta_t, lambda_t, mu_t, I_t, v1, v2, v3, v4, v5, dt, Hb, H0, L, dR_dt, lambda_bar, (2/dt)*mu_bar, g, rho, Mass, solvers_print);
phi_solver1 = solver_phi(phi1, phi0_5, phi0_5, eta1, LM_lam0_5, etaR, phi, v, 0.5*dt, g, solvers_print);            # Give eta1 instead of eta0 and phi0_5 instead of phi0

phi0_5, eta1, LM_lam0_5, LM_mu0_5, I = w.split()

h.assign(Hb+eta0)
lambda0_5.assign(lambda_bar + (2/dt)*LM_lam0_5)
mu0_5.assign(mu_bar + LM_mu0_5)

# Write data to files
output_file = File('Results/results.pvd')
output_file.write(phi0, eta0, h, lambda0_5, mu0_5, time=t)

buoy_file = open("Results/buoy_motion.txt", "w")
plot_buoy(t, Z0, W0, buoy_file);

frame_number = 0
write_to_vtk_buoy(0.9*Lp, L, H0-sqrt(2*Mass*tan(a)/rho)-tan(a)*(0.9*Lp-L), Hb.dat.data[Nx], "Triangle Buoy", frame_number);
write_to_vtk_waterline(float(xp), H0, "Waterline", frame_number);

# Initial energy
E0_w = 0.5*rho*assemble( (Hb*abs(grad(phi0))**2 + g*eta0**2)*dx )   # Energy of the water
E0_b = 0.5*Mass*float(W0)**2										# Energy of the buoy
E0   = E0_w + E0_b				                                    # Total energy: water + buoy

E_file = open("Results/energy.txt", "w")
E = find_energy(phi0, eta0, W0, Z0, Hb, rho, g, Mass, t, E0, E_file);

while(t < T-dt):
    print t, E

    # Time update of sluice gate etaR and wave maker R(t) = A/w*(1-cos(w*t))
    etaR.interpolate(Expression("0"))
    dR_dt.interpolate(Expression("0"))

    if (sluice_gate == 'True' and t < Ts):
        etaR_expr.t = t+0.5*dt
        etaR_interpolator.interpolate()
    elif wave_maker == 'True':
        dR_dt_expr.t = t+0.5*dt
        dR_dt_interpolator.interpolate()

    t += dt

	# Solve the variational problem
    (phi0, eta0, W0, Z0, LM_lam0_5, LM_mu0_5) = solvers_SV(w, F_solver, phi_solver1, eta0, phi0, Z0, W0, eta1, phi1, Z1, W1, W0_5, rho, Mass, dt);

    h.assign(Hb+eta0)
    lambda0_5.assign(lambda_bar + (2/dt)*LM_lam0_5)
    mu0_5.assign(mu_bar + LM_mu0_5)

    # Plot buoy
    frame_number = frame_number + 1
    write_to_vtk_buoy(0.9*Lp, L, H0-sqrt(2*Mass*tan(a)/rho)-tan(a)*(0.9*Lp-L)+eta0.dat.data[Np], Hb.dat.data[Nx]+eta0.dat.data[Nx], "Triangle Buoy", frame_number);

    # Calculate and plot waterline point
    xp.assign(Lp + (Z0 - eta0.dat.data[Np])/tan(a))
    write_to_vtk_waterline(float(xp), H0+eta0.dat.data[Np], "Waterline", frame_number);

	# Monitor energy in time
    if sluice_gate == 'True':
        if (t > Ts and set_E0 == 'True'):    # As soon the sluice gate is released (i.e. t>Ts), calculate E0 again
    	    E0 = 0.5*rho*assemble( (Hb*abs(grad(phi0))**2 + g*eta0**2)*dx ) + 0.5*Mass*float(W0)**2
            set_E0 = 'False'

    E = find_energy(phi0, eta0, W0, Z0, Hb, rho, g, Mass, t, E0, E_file);

    # Write data to files
    output_file.write(phi0, eta0, h, lambda0_5, mu0_5, time=t)
    plot_buoy(t, Z0, W0, buoy_file);

E_file.close()
buoy_file.close()

