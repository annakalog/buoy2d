
from firedrake import *


##################################################

def plot_buoy(t, Z0, W0, buoy_file):

    time = str(t)
    Z = str(float(Z0))
    W = str(float(W0))
    buoy_file.write('%-10s %-10s %-10s\n' % (time,Z,W))

    return;

##################################################
