
from firedrake import *


##################################################

def write_to_vtk_buoy(xp, L, Hb_xp, Hb_L, title, frame_number):
    
    # Open the output file
    xp_file = open("Results/buoy_object_" + str(frame_number) + ".vtk", "w")
    
    #  Write the data
    xp_file.write('# vtk DataFile Version 2.0\n')
    xp_file.write('%-s\n' % title)
    xp_file.write('ASCII\n')
    xp_file.write('\n')
    
    xp_file.write('DATASET UNSTRUCTURED_GRID\n')
    xp_file.write('POINTS %-s double\n' % str(6))
    xp_file.write('%-s %-s %-s\n' % (str(xp),str(0),str(Hb_xp)))
    xp_file.write('%-s %-s %-s\n' % (str(L),str(0),str(Hb_L)))
    xp_file.write('%-s %-s %-s\n' % (str(L),str(0),str(Hb_xp)))
    xp_file.write('%-s %-s %-s\n' % (str(xp),str(1),str(Hb_xp)))
    xp_file.write('%-s %-s %-s\n' % (str(L),str(1),str(Hb_L)))
    xp_file.write('%-s %-s %-s\n' % (str(L),str(1),str(Hb_xp)))
    
    xp_file.write('\n')
    xp_file.write('CELLS %-s %-s\n' % (str(1),str(7)))
    xp_file.write('%-s %-s %-s %-s %-s %-s %-s' % (str(6),str(0),str(1),str(2),str(3),str(4),str(5)))
    xp_file.write('\n')
    
    # VTK has a cell type 13 for wedges (trianglular prism)
    xp_file.write('\n')
    xp_file.write('CELL_TYPES %-s\n' % str(1))
    xp_file.write('%-s\n' % str(13))
    
    xp_file.close()
    
    return;

##################################################

def write_to_vtk_waterline(xp, h_xp, title, frame_number):
    
    # Open the output file
    xp_file = open("Results/waterline_" + str(frame_number) + ".vtk", "w")
    
    #  Write the data
    xp_file.write('# vtk DataFile Version 2.0\n')
    xp_file.write('%-s\n' % title)
    xp_file.write('ASCII\n')
    xp_file.write('\n')
    
    xp_file.write('DATASET UNSTRUCTURED_GRID\n')
    xp_file.write('POINTS %-s double\n' % str(2))
    xp_file.write('%-s %-s %-s\n' % (str(xp),str(0),str(h_xp)))
    xp_file.write('%-s %-s %-s\n' % (str(xp),str(1),str(h_xp)))
    
    xp_file.write('\n')
    xp_file.write('CELLS %-s %-s\n' % (str(1),str(3)))
    xp_file.write('%-s %-s %-s' % (str(2),str(0),str(1)))
    xp_file.write('\n')
    
    # VTK has a cell type 1 for vertex (point)
    xp_file.write('\n')
    xp_file.write('CELL_TYPES %-s\n' % str(1))
    xp_file.write('%-s\n' % str(3))
    
    xp_file.close()
    
    return;

##################################################
